
public class Reservation {
	
	//##########################################
	//########		PUBLIC METHODS		########
	//##########################################
	
	/**
	 * Retrieve the day number in the year
	 * @return date is the number of the day in the year
	 */
	public int getDate() {
		return date;
	}
	
	/**
	 * Set the day in the year the reservation itself
	 * @param date is when the reservation begins
	 */
	public void setDate(int date) {
		this.date = date;
	}
	
	/**
	 * Retrieve the number of nights the reservation is active for
	 * @return nights is how many nights were requested
	 */
	public int getNights() {
		return nights;
	}
	
	/**
	 * Store the number of nights the guest wants to stay
	 * @param nights is the number of nights the guest wants to stay
	 */
	public void setNights(int nights) {
		this.nights = nights;
	}
	
	/**
	 * Get the name of the guest who made this reservation
	 * @return the name of the person making the booking
	 */
	public String getGuest(){
		return guest;
	}
	
	/**
	 * Set the name of the guest who made this reservation
	 * @param name
	 */
	public void setGuest(String name){
		this.guest = name;
	}
	
	/**
	 * Return the QTY of a certain room type requested
	 * @param type of room requested
	 * @return QTY of <type> room requested
	 */
	public int getRoomQTY(int type){
		int roomQTY = 0;
		switch(type){
		case 1 : 	roomQTY = singles;
					break;
		case 2 : 	roomQTY = doubles;
					break;
		case 3 : 	roomQTY = triples;
					break;
		default:	break;	
		}
		return roomQTY;
	}
	
	/**
	 * Add to the reservation's List the number of a certain type
	 * of room that was requested
	 * @param type of room requested
	 * @param QTY of <type> room requested
	 */
	public void setRoomQTY(int type, int QTY){
		switch(type){
			case 1 : 	this.singles = QTY;
						break;
			case 2 : 	this.doubles = QTY;
						break;
			case 3 : 	this.triples = QTY;
						break;
			default:	break;
				
		}
	}
	
	/**
	 * Flag if the reservation object was made 
	 * 		for the purpose of a change
	 * @param flag True for change, else false 
	 */
	public void setForChange(boolean flag){
		forChange = flag;
	}
	
	/**
	 * Indicate if a reservation was made for change or not
	 * @return True for change, else false
	 */
	public boolean isForChange(){
		return forChange;
	}
	
	/**
	 * Set the order in which the bookings for the different
	 * 		room types get printed
	 * @param type The room type
	 * @param pos When it will be printed. See Accomodation : void applyBook() method
	 */
	public void setReqOrder(String type, int pos){
		if (type.equals("single")){
			posForSing = pos;
		} else if (type.equals("double")){
			posForDoub = pos;
		} else if (type.equals("triple")){
			posForTrip = pos;
		}
	}
	
	/**
	 * Indicates the position the bookings of a certain room type will be printed
	 * 		when they are made. See Accomodation : void applyBook() method
	 * @param type Which room type needs a position
	 * @return The positions the room type has
	 */
	public int getReqPos(String type){
		if (type.equals("single")){
			return posForSing;
		} else if (type.equals("double")){
			return posForDoub;
		} else {
			return posForTrip;
		}
	}
	
	//##########################################
	//########			FIELDS			########
	//##########################################
	
	
	private int date = -1;
	private int nights = 0;
	private String guest = null;
	private int singles = 0;
	private int doubles = 0;
	private int triples = 0;
	private int posForSing = -1;
	private int posForDoub = -1;
	private int posForTrip = -1;
	private boolean forChange = false;
}
