//import java.util.*;
public interface Hotel {
	
	/**
	 * Gets the name of the hotel 
	 * @return the name of the hotel 
	 */
	public String getName();
	
	/**
	 * Prints the all the reservation for this hotel in order 
	 * of room and then date
	 */
	public void printReservations();
	
	/**
	 * Create an ArrayList of all rooms in the hotel with 
	 * capacity the same as type
	 * @param type of room looking for
	 * @return ArrayList of Room that match type
	 */
	//public ArrayList<Room> findRoom(int type); ##NOT YET REMOVED FROM MAIN CODE: DO MORE TESTING
	
	/**
	 * Makes a new room in the accomodation
	 * @param number The room number
	 * @param type The type of room 
	 */
	public void makeRoom(int number,int type);
	
	/**
	 * Attempts to make a reservation
	 * @param R The reservation information 
	 * @return boolean True if success or false for failure
	 */
	public boolean parseReservation(Reservation R);
	
	/**
	 * Attempts to perform a cancellation on an existing booking
	 * @param C Has all the cancellation details
	 * @return True if successful, else false
	 */
	public boolean parseCancellation(Cancellation C);
	
	/**
	 * Indicates if a hotel has a particular room
	 * @param number Room is identified by room number
	 * @return True if room exists, else false
	 */
	public boolean hasRoom(int number);

}
