
public class Cancellation {

	/**
	 * Retrieve original reservation day
	 * @return int The reservation day
	 */
	public int getDate() {
		return date;
	}
	
	/**
	 * Store the original reservation day
	 * @param date is the original reservation day
	 */
	public void setDate(int date) {
		this.date = date;
	}
	
	/**
	 * Get the number of nights the original reservation was for
	 * @return the original number of booked nights
	 */
	public int getNights() {
		return nights;
	}
	
	/**
	 * Set the number of nights the original reservation was for
	 * @param nights is the original number of booked nights
	 */
	public void setNights(int nights) {
		this.nights = nights;
	}
	
	/**
	 * Get the name of the guest making the cancellation request
	 * @return the name of the guest cancelling
	 */
	public String getGuest() {
		return guestName;
	}
	
	/**
	 * Set the name of the guest making the cancellation request
	 * @param guestName the name of the guest cancelling
	 */
	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}
	
	/**
	 * Get the room number of the room to be cancelled
	 * @return the number of the room to be cancelled
	 */
	public int getNum() {
		return roomNum;
	}
	/**
	 * Set the number of the room to be cancelled
	 * @param roomNum the number of the room to be cancelled
	 */
	public void setRoomNum(int roomNum) {
		this.roomNum = roomNum;
	}

	public void setForChange(boolean type){
		forChange = type;
	}
	
	public boolean isForChange(){
		return forChange;
	}
	
	private int date = -1;
	private int nights = 0;
	private String guestName = null;
	private int roomNum = 0;
	private boolean forChange = false;
}
