import java.util.Calendar;

public class Room implements Comparable<Room>{
	
	//##########################################
	//########		CONSTRUCTOR			########
	//##########################################
	/**
	 * Make a new room with a room number and type
	 * @param number Is the room number
	 * @param type Is the capacity of the room
	 */
	public Room(int number, int type){
		this.roomNum = number;
		this.type = type;
		availability = new String[DAYS_IN_A_YEAR + 1];			// BECAUSE CALENDAR CONSIDERS JAN 1
	}															// AS 1 AND NOT 0
	
	//##########################################
	//########		PUBLIC METHODS		########
	//##########################################
	
	
	/**
	 * Gives back room number
	 * @return The room number
	 */
	public int getNum(){
		return roomNum;
	}
	
	/**
	 * Give back the room type
	 * @return This is the room type
	 */
	public int getType(){
		return type;
	}
	
	/**
	 * Registers a booking in the room
	 * @param R The reservation to be made in the room
	 */
	public void makeBook(Reservation R){
		int nights = R.getNights();
		int DoY = R.getDate();
		String name = R.getGuest();
		for(int i = 0; i < nights; i++){					// WRITING A NAME INTO A POSITION
			availability[DoY+i] = name;						// INDICATES THAT THE ROOM IS BOOKED 
		}													// FOR THAT PERSON FOR THAT DAY
	}
	
	/**
	 * Cancels a booking in a room
	 * @param C Holds the cancellation details
	 * @return True for successful cancellation else, false
	 */
	public boolean cancelBook(Cancellation C){
		int DoY = C.getDate();
		int nights = C.getNights();
		String name = C.getGuest();
		boolean resultCode = true;
		for (int i = 0; i < nights; i++){
			if (availability[DoY+i] == null || !(availability[DoY+i].equals(name))){
				resultCode = false;								// IF THE PERSON WASNT THERE TO BEGIN WITH
				break;											// OR WE GET SOMEONE ELSES NAME OVER THE TIME FRAME
			}
		}
		if ((resultCode != false) && !(C.isForChange())){		// IF GOOD TO DELETE THEN DELETE 
			for (int i = 0; i < nights; i++){					
				availability[DoY+i] = null;
			}
		}
		return resultCode;
	}
	
	/**
	 * Determines if a a room is a available for a given reservation
	 * @param date Starting date for reservation
	 * @param nights Number of nights for reservation
	 * @param name Guest making the booking
	 * @param forResChange If the availability concerns a change // DOESNT MAKE SENSE
	 * @return True if available, else false
	 */
	public boolean isAvail(int date,int nights,String name,boolean forResChange){
		boolean resultCode = true;

		for(int i = 0; i < nights; i++){
			if(!(availability[date+i] == null 				// IF NOT BOOKING THERE, OR SAME 
					|| availability[date+i].equals(name))){		// PERSON HAS THAT SPACE ITS OK
				resultCode = false;
				break;
			} 
		}
		return resultCode;
	}
	
	/**
	 * Prints all the bookings for the room
	 * @param hotelName For the purpose of printing out the hotel name //MIGHT WANT TO LOOK INTO THAT
	 */
	public void printBookings(String hotelName){
		String month;
		int DoM;
		int date;
		int nights = 0;
		String name;
		int i = 0;
		
		while(i < DAYS_IN_A_YEAR+1){						// UGLIEST PART OF CODE
			if (availability[i] != null){
				System.out.print(hotelName+" ");
				date = i;									// IF WE MEET A BOOKING AND HENCE NAME
				name = availability[i];						// MARK START BOOKING START DATE
				
				while(i < DAYS_IN_A_YEAR+1					// WE ITERATE ALONG UNTIL WE CAN'T SEE
						&& availability[i]!= null 			// THE NAME ANYMORE, AND INCREMENT THE 
						&& availability[i].equals(name)	){	// NUMBER OF NIGHTS THAT PERSON HAS STAYED
					nights++;
					i++;
				}
				
				intToDate(date);							// WE START CONVERTING THE BOOKING START
				month = dateMonth();						// NUMBER INTO A MONTH AND DAY OF MONTH
				DoM = dateDoM();
				System.out.println(this.roomNum+" "+month+" "+DoM+" "+nights+" "+name);
				nights = 0;
			} else {
				i++;										// KEEP GOING UNTIL WE FIND A NAME
			}
		}
	}
	
	@Override
	/**
	 * Room comparisons will be done via room number
	 * @param r Another room to compare against current Room object
	 * @return A positive, negative or 0 number based upon comparison
	 */
	public int compareTo(Room r){					
		return (this.getNum() - r.getNum());					
	}
	
	//##########################################
	//########		PRIVATE METHODS		########
	//##########################################	
	
	
	/**
	 * Set up an number to be converted into a date
	 * @param date The number to be converted
	 */
	private void intToDate(int date){
		cal.set(Calendar.DAY_OF_YEAR, date);
	}
	
	/**
	 * Gives back the month associated with the number set by intToDate(int)
	 * @return The String version of the month in 3-characters
	 */
	private String dateMonth(){
		int monthNum = cal.get(Calendar.MONTH);
		String monthName;
		switch(monthNum){
			case 0 : monthName = "Jan";
					 break;
			case 1 : monthName = "Feb";
					 break;
			case 2 : monthName = "Mar";
			 		 break;
			case 3 : monthName = "Apr";
			         break;
			case 4 : monthName = "May";
	 		 		 break;
			case 5 : monthName = "Jun";
	         		 break;
			case 6 : monthName = "Jul";
					 break;
			case 7 : monthName = "Aug";
					 break;
			case 8 : monthName = "Sep";
					 break;
			case 9 : monthName = "Oct";
			 		 break;
			case 10 : monthName = "Nov";
			 		 break;
			case 11 : monthName = "Dec";
	 		 		 break;
	 		default : monthName = null;
	 				 break;		
		}
		return monthName;
	}
	
	/**
	 * Gives back the day of a month associated with the number set by intToDate
	 * @return The day in a month
	 */
	private int dateDoM(){
		return cal.get(Calendar.DAY_OF_MONTH);
	}
	
	//##########################################
	//########			FIELDS			########
	//##########################################
	
	private int roomNum;
	private Calendar cal = Calendar.getInstance();	
	private int type;
	private String[] availability;
	private static final int DAYS_IN_A_YEAR = 365;
}
