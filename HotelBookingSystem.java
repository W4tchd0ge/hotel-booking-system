import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

public class HotelBookingSystem{
	
	public static void main(String[] args){
		HotelBookingSystem HBS = new HotelBookingSystem();
		Scanner commands = null;
		
		try
    	{
        	commands = new Scanner(new FileReader(args[0]));
    	}
    	catch (FileNotFoundException e) {}

		while (commands.hasNextLine()) {
			String request = commands.nextLine();
			HBS.TitanBookSys(request);
		}
		commands.close();
	}
	
	
	//##########################################
	//########		PUBLIC METHODS		########
	//##########################################
	
	/**
	 * Extrapolates a string with a COMMAND and corresponding data to make
	 * a BOOKING, CANCELLATION, CHANGE, or PRINT
	 * @param reqLine This is the string that contains the execution
	 * 			command and corresponding information
	 */
	private void TitanBookSys(String reqLine){

		request = reqLine.split(" ");
		
		if(request[COMMAND].equals("Hotel")){
			this.makeSpace(request[HOTEL], request[ROOM], request[CAPACITY]);
			
		} else if (request[COMMAND].equals("Book")){
			this.goBooking(request,NOT_FOR_CHANGE);
		
		} else if (request[COMMAND].equals("Cancel")){
			if (this.goCancel(request,NOT_FOR_CHANGE)){
				System.out.println("Reservation cancelled");			// MASSIVE HACK TO ALLOW THIS PRINT
			}															// TO HAPPEN WITHOUT BEING AFFECTED BY
																		// BEING CALLED FOR GOCHANGE LATER
		} else if (request[COMMAND].equals("Change")){
			this.goChange(request);
			
		} else if (request[COMMAND].equals("Print")){
			this.printEverything(request[HOTEL]);
		}
	}

	
	//##########################################
	//########		PRIVATE METHODS		########
	//##########################################	
	
	
	/**
	 * COMMAND(Hotel) makes new hotel and/or room
	 * @param hotelName name of specified hotel
	 * @param room The number of room in hotelName
	 * @param capacity Of the room
	 */
	private void makeSpace(String hotelName, String room, String capacity){
		int roomNum = Integer.parseInt(room);
		int size = Integer.parseInt(capacity);
		
		Hotel selected = null;						// LABELS OUR HOTEL OF INTEREST
		
		for(Hotel h : hotels){						// GO THROUGH AND FIND APPLICABLE HOTEL
			if (h.getName().equals(hotelName)){
				selected = h;
				break;
			}
		}
		
		if (selected == null){						// HOTEL WASNT FOUND SO MAKE IT APPEAR
			Hotel newPlace = new Accomodation(hotelName);
			hotels.add(newPlace);
			selected = newPlace;
		}
		
		if (!selected.hasRoom(roomNum)){			// IF HOTEL DOESNT HAVE SAID ROOM MAKE IT APPEAR
			selected.makeRoom(roomNum, size);
		}
	}
	
	/**
	 * Attempts to process a booking and prints message if failed
	 * @param request Has the relevant details for booking to be stored. 
	 * 			See makeResForm()
	 * @param forChange Flag for indicating if this booking is for 
	 * 			amendment purposes
	 * @return True for success else false for failure
	 */
	private boolean goBooking(String[] request, boolean forChange){
		boolean resultCode = false;
		Reservation newRes = makeResForm(request);
		if (forChange == true){										// IF BOOKING IS FOR CHANGE THEN 
			newRes.setForChange(forChange);							// SET A FLAG IN THE RESERVATION TO BE 
		}															// CONSIDERED LATER BY OTHER METHODS
		
		for (Hotel h : hotels){										// PING EACH HOTEL FOR AVAILABILITES
			if (h.parseReservation(newRes)){
				resultCode = true;
				break;
			}
		}
		if (!resultCode && !forChange){
			System.out.println("Booking rejected");
		}
		return resultCode;
	}
	
	/**
	 * COMMAND(Cancel) : executes a cancellation for a specific room 
	 * @param request Contains the details for cancellation
	 * @return boolean True for success, else false for failure
	 */
	private boolean goCancel(String[] request, boolean forChange){
		boolean resultCode = false;
		Cancellation C = makeCancelForm(request);
		if (forChange == true){											// IF BOOKING IS FOR CHANGE THEN 
			C.setForChange(forChange);									// SET A FLAG IN THE RESERVATION TO BE
		}																// CONSIDERED LATER BY OTHER METHODS
		String hotelToCancel = request[HOTEL_CANCEL];								

		for(Hotel h : hotels){											// FIND HOTEL TO CANCEL FROM
			if (h.getName().equals(hotelToCancel)){						// ONCE WE FIND IT, MAKE THE
				if(h.parseCancellation(C)){								// CANCELLATION 
					resultCode = true;
				}
			}
		}
		if (!resultCode && !forChange){									// OR PRINT 
			System.out.println("Cancellation rejected");
		}
		return resultCode;
	}
	
	/**
	 * COMMAND(Change) : executes a booking amendment for a guest
	 * @param request Contains details for amendment
	 * @return boolean True for success, else false for failure
	 */
	private boolean goChange(String[] chgReq){
		boolean resultCode = false;

		//####NEW RESERVATION FOR CHANGE####
		String[] resReq = new String[NUM_BOOK_DETAIL];
		resReq[0] = "Book";
		resReq[1] = chgReq[1];
		for(int i = 2, j = 7; i < 6 ; i++,j++){					// SOME OF POSITIONS OF DETAILS LINE
			resReq[i] = chgReq[j];								// UP TO A DEGREE
		}
		resReq[6] = "1";
		
		//####NEW CANCELLATION FOR CHANGE####
		String[] cancelReq = new String[NUM_CANCEL_DETAIL];
		cancelReq[0] = "Cancel";
		for(int i = 1; i < 7 ; i++){
			cancelReq[i] = chgReq[i];
		}
		
		if (goCancel(cancelReq,FOR_CHANGE) && goBooking(resReq, FOR_CHANGE)){		// IF CANCELLATION AND NEW BOOKING 
			goCancel(cancelReq,NOT_FOR_CHANGE);										// THAT MAKE UP RESERVATION CAN HAPPEN
			goBooking(resReq, NOT_FOR_CHANGE);
			resultCode = true;
		} else {
			System.out.println("Change rejected");
		}
		
		return resultCode;
	}
	
	/**
	 * COMMAND(Print) : print all reservations for a hotel in order of room no.
	 * 						then date
	 * @param hotelName Is the hotel to have its reservations printed
	 */
	private void printEverything(String hotelName){
		for(Hotel h : hotels){
			if(h.getName().equals(hotelName)){
				h.printReservations();
				break;
			}
		}
	}
	
	/**
	 * Turn a date into a number between 1 and 365
	 * @param month The month to be converted into the number
	 * @param date The day of the month to be converted into the number 
	 * @return The number representing a day in the year
	 */
	private int setDay (String month,int date){							// STUPID THING WOULDNT 
       if (month.equals("Jan")){										// LET ME USE A STRING 
            cal.set(Calendar.MONTH, Calendar.JANUARY);					// FOR THE SWITCH CONDI
            cal.set(Calendar.DAY_OF_MONTH, date);						// HAVE TO USE A DAMN INT
            
       } else if (month.equals("Feb")){
            cal.set(Calendar.MONTH, Calendar.FEBRUARY);
            cal.set(Calendar.DAY_OF_MONTH, date);
            
       } else if (month.equals("Mar")) {  
            	cal.set(Calendar.MONTH, Calendar.MARCH);
            	cal.set(Calendar.DAY_OF_MONTH, date);
            	
       } else if (month.equals("Apr")) {  
            	cal.set(Calendar.MONTH, Calendar.APRIL);
            	cal.set(Calendar.DAY_OF_MONTH, date);
            	
       } else if (month.equals("May")) {  
            	cal.set(Calendar.MONTH, Calendar.MAY);
            	cal.set(Calendar.DAY_OF_MONTH, date);        
            	
       } else if (month.equals("Jun")) {  
            	cal.set(Calendar.MONTH, Calendar.JUNE);
            	cal.set(Calendar.DAY_OF_MONTH, date);      
            	
       } else if (month.equals("Jul")) {  
            	cal.set(Calendar.MONTH, Calendar.JULY);
            	cal.set(Calendar.DAY_OF_MONTH, date);    
            	
       } else if (month.equals("Aug")) {  
            	cal.set(Calendar.MONTH, Calendar.AUGUST);
            	cal.set(Calendar.DAY_OF_MONTH, date);
                
       } else if (month.equals("Sep")) {  
            	cal.set(Calendar.MONTH, Calendar.SEPTEMBER);
            	cal.set(Calendar.DAY_OF_MONTH, date);
                
       } else if (month.equals("Oct")) { 
            	cal.set(Calendar.MONTH, Calendar.OCTOBER);
            	cal.set(Calendar.DAY_OF_MONTH, date);
                
       } else if (month.equals("Nov")) {
            	cal.set(Calendar.MONTH, Calendar.NOVEMBER);
            	cal.set(Calendar.DAY_OF_MONTH, date);
                
       } else if (month.equals("Dec")) { 
            	cal.set(Calendar.MONTH, Calendar.DECEMBER);
            	cal.set(Calendar.DAY_OF_MONTH, date);
       } else {
    	   return -1;	// WE TO FORCE A ERROR IF SOMETHING ELSE IS GIVIN IN
       }

       return cal.get(Calendar.DAY_OF_YEAR);
	}
	
	/**
	 * Create a new cancellation object and fill in the fields
	 * @param request Contains the cancellation details
	 * @return The filled in cancellation object
	 */
	private Cancellation makeCancelForm(String[] request){
		int day = setDay(request[MONTH_CANCEL],Integer.parseInt(request[DATE_CANCEL]));
		Cancellation cancel = new Cancellation();
		
		cancel.setGuestName(request[NAME]);									// INTIALISE THE CANCELLATION
		cancel.setDate(day);												// DETAILS : GUEST NAME, DATE
		cancel.setRoomNum(Integer.parseInt(request[ROOM_CANCEL]));			// ROOM NUMBER, NIGHTS
		cancel.setNights(Integer.parseInt(request[NIGHT_CANCEL]));
		
		return cancel;
	}
	
	/**
	 * Create a new reservation object
	 * @param request Contains the reservation details
	 * @return The filled in reservation object
	 */
	private Reservation makeResForm(String[] request){
		Reservation newRes = new Reservation();
		int N = request.length;

		int day = setDay(request[MONTH],Integer.parseInt(request[DATE]));
		newRes.setGuest(request[NAME]);												// FILLING OUT NAME, DATE AND NIGHTS
		newRes.setNights(Integer.parseInt(request[NIGHTS]));						// OF RESERVATION
		newRes.setDate(day);
		
		for(int i = 5, pos = 0 ; i < N; i+=2,pos++){								// FILLING OUT ROOM TYPES AND QTY
			if (request[i].equals("single")){										// POS REFERS TO IN WHAT ORDER THE 
				newRes.setRoomQTY(SINGLE, Integer.parseInt(request[i+1]));			// THE BOOKINGS FOR EACH TYPE WILL
				newRes.setReqOrder("single", pos);									// BE PRINTED
				
			} else if (request[i].equals("double")){
				newRes.setRoomQTY(DOUBLE, Integer.parseInt(request[i+1]));
				newRes.setReqOrder("double", pos);
				
			} else if (request[i].equals("triple")){
				newRes.setRoomQTY(TRIPLE, Integer.parseInt(request[i+1]));
				newRes.setReqOrder("triple", pos);
			}
		}
		
		return newRes;
	}
	
	//##########################################
	//########			FIELDS			########
	//##########################################
	
	private ArrayList<Hotel> hotels = new ArrayList<Hotel>();
	private String[] request;
	private Calendar cal = Calendar.getInstance();
	private static final int COMMAND 			= 0;
	private static final int SINGLE 			= 1;
	private static final int NAME 				= 1;
	private static final int HOTEL 				= 1;
	private static final int HOTEL_CANCEL 		= 2;
	private static final int ROOM 				= 2;
	private static final int DOUBLE 			= 2;
	private static final int MONTH 				= 2;
	private static final int DATE 				= 3;
	private static final int TRIPLE 			= 3;
	private static final int CAPACITY 			= 3;
	private static final int ROOM_CANCEL 		= 3;
	private static final int MONTH_CANCEL 		= 4;
	private static final int NIGHTS 			= 4;
	private static final int DATE_CANCEL 		= 5;
	private static final int NIGHT_CANCEL 		= 6;
	private static final int NUM_BOOK_DETAIL 	= 7;
	private static final int NUM_CANCEL_DETAIL 	= 7;
	private static final boolean FOR_CHANGE 	= true;
	private static final boolean NOT_FOR_CHANGE = false;
}