import java.util.*;

public class Accomodation implements Hotel{
	
	//##########################################
	//########		CONSTRUCTOR			########
	//##########################################
	/**
	 * Create a new accomodation object with a name
	 * @param name Is to be name of the accomodation
	 */
	public Accomodation (String name){
		this.hotelName = name;
	}
	
	//##########################################
	//########		PUBLIC METHODS		########
	//##########################################
	

	public String getName(){
		return hotelName;
	}

	public boolean hasRoom(int number){
		for(Room r : rooms){
			if (r.getNum() == number){
				return true;
			}
		}
		return false;
	}

	public void makeRoom(int number, int type){
		Room newRoom = new Room(number, type);
		rooms.add(newRoom);
	}
	
	public boolean parseReservation(Reservation R){

		boolean resultCode = false;										
		int singReq = R.getRoomQTY(SINGLE);							// HOW MANY SINGLES REQUESTED
		ArrayList<Room> singles = availRooms(R, SINGLE);			// HOW MANY FREE SINGLES ROOMS FOR TIME FRAME
	
		int doubReq = R.getRoomQTY(DOUBLE);							// SAME WITH DOUBLE
		ArrayList<Room> doubles = availRooms(R, DOUBLE);
		
		int tripReq = R.getRoomQTY(TRIPLE);							// SAME WITH TRIPLE
		ArrayList<Room> triples = availRooms(R, TRIPLE);
		
		if (singReq <= singles.size() && doubReq <= doubles.size()	// IF NO. WANTED <= NO.AVAILABLE FOR ALL TYPES
				&& tripReq <= triples.size()){
			if (!R.isForChange()){									// RESRICTS PRINTING IF THIS METHOD USED
				System.out.print(hotelName);						// FOR PURPOSE OF CHANGING A BOOKING
			}
			if (!R.isForChange()){
				for(int i = 0 ; i < 3 ; i++){							// LOOP DETERMINES ORDER OF THE BOOKINGS FOR
					if (R.getReqPos("single") == i){					// DIFFERENT TYPES OF ROOMS GETS PRINTED 
						applyBook(singReq,singles,R);
					} else if (R.getReqPos("double") == i){
						applyBook(doubReq,doubles,R);
					} else if (R.getReqPos("triple") == i){
						applyBook(tripReq,triples,R);
					}
				}
			}
			resultCode = true;
			if (!R.isForChange()){
				System.out.print("\n");
			}
		}
		//System.out.println(resultCode);
		return resultCode;
	}
	
	public boolean parseCancellation(Cancellation C){
		
		Room suite = null;
		boolean resultCode = false;
		for (Room r : rooms){
			if (r.getNum() == C.getNum()){							// FIND THE ROOM TO MAKE THE CANCELLATION
				suite = r;
				break;
			}
		}
		if (suite != null){
			resultCode = suite.cancelBook(C);						// ATTEMPT TO MAKE CANCELLATION
		}
		return resultCode;
	}
	
	public void printReservations(){
		orderRooms();
		for(Room r : rooms){
			r.printBookings(hotelName);
		}
	}

	
	//##########################################
	//########		PRIVATE METHODS		########
	//##########################################
	
	
	/**
	 * Finds all rooms in the hotel available 
	 * for a corresponding reservation
	 * @param res Holds the details for the reservation
	 * @param type Room type requested
	 * @return An ArrayList of all the available rooms
	 */
	private ArrayList<Room> availRooms(Reservation res, int type){
		ArrayList<Room> available = new ArrayList<Room>();
		for(Room r : rooms){										// FIND ALL ROOMS AVAILABLE FOR THE GIVEN
			if(r.getType() == type){								// RESERVATION DETAILS AND TYPE OF ROOM
				if (r.isAvail(res.getDate(), res.getNights(),
						res.getGuest(),res.isForChange())){
					available.add(r);
				}
			}
		}
		return available;
	}
	
	/**
	 * Applies (part of) the booking on a list of rooms of the same type
	 * @param QTY The number of rooms requested 
	 * @param availRooms The list of available rooms of one type
	 * @param R The reservation details
	 */
	private void applyBook(int QTY, ArrayList<Room> availRooms,Reservation R){	
		Room temp;
		for(; QTY != 0; QTY--){
			temp = availRooms.remove(0);					// REMOVE FROM THE LIST OF AVAILABLE ROOMS
			temp.makeBook(R);								// AND REGISTER THE BOOKING FOR EACH ROOM
				System.out.print(" "+temp.getNum());
		}
	}

	/**
	 * Sorts the rooms in ascending order by room number
	 */
	private void orderRooms(){
		if (rooms.size() > 1){
			Collections.sort(rooms);
		}
	}
	
	
	//##########################################
	//########			FIELDS			########
	//##########################################
	
	
	private String hotelName;
	private ArrayList<Room> rooms = new ArrayList<Room>();
	private static final int SINGLE = 1;
	private static final int DOUBLE = 2;
	private static final int TRIPLE = 3;
}
